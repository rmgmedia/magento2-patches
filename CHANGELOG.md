RMG Media Magento 2 Patches
===========================

## Deprecated

This CHANGELOG file is no longer maintained. The nature of this repo being only patches does not
really make sense to continue to tag as we add patches.


## 2.0.0

* Updated namespace from `rmgmedia` to `rmg`

## 1.0.2

* Fixed cwd setting for:
** Fixed_method_chaining_contract_for_Product_Collection_composer-2019-10-18-07-02-37.patch
** Catalog_pagination_issue_on_Elasticsearch_6_composer-2019-10-11-08-07-41.patch  

## 1.0.1

* Removed magento/magento2-functional-testing-framework target
* Updated README

## 1.0.0

* Initial release
