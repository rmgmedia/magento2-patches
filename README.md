RMG Media Magento 2 Patches
===========================

## Purpose:
@todo Add purpose and reasoning behind this over other M2 patch libraries

## How to Use:

### Add Composer repositories, if not already included.

```bash
composer config repositories.magento composer https://repo.magento.com
composer config repositories.rmg/magento2-modules composer https://composer.rmgmedia.com/magento2/module
```

### Optionally add patch configurations.

```json
{
    "extra": {
        "magento-force": "override",
        "patcher": {
            "appliers": {
                "GIT": {
                    "ping": "[[bin]] rev-parse --is-inside-work-tree"
                }
            }
        },
        "patcher-windows": {
            "appliers": {
                "PATCH": {
                    "bin": "where patch /F"
                },
                "GIT": {
                    "ping": "[[bin]] rev-parse --is-inside-work-tree",
                    "bin": "where git /F"
                }
            }
        }
    }
}
```

### Require the composer package.

```bash
composer require rmg/magento2-patches:^2.0.0
```

#### If you have already included the package, then update it with this command

```bash
composer update rmg/magento2-patches --with-dependencies
```

### Example Output:

```
  - Installing rmg/magento2-patches (dev-master e53bca5): Downloading (100%)
Package phpunit/phpunit-mock-objects is abandoned, you should avoid using it. No replacement was suggested.
Writing lock file
Generating autoload files
Processing patches configuration
  - Applying patches for magento/module-catalog (1)
    ~ rmg/magento2-patches: patches/Fixed_method_chaining_contract_for_Product_Collection_composer-2019-10-18-07-02-37.patch [NEW]
      Fixed method chaining contract for Product Collection patch for Magento 2.3.3

  - Applying patches for magento/module-elasticsearch (1)
    ~ rmg/magento2-patches: patches/Catalog_pagination_issue_on_Elasticsearch_6_composer-2019-10-11-08-07-41.patch [NEW]
      Resolve ElasticSearch 6 Issues

Writing patch info to install file
```

### Validate patches applied

```bash
composer patch:list
```

#### Example output:

```bash
PS> composer patch:list
magento/module-catalog
  ~ rmg/magento2-patches: patches/Fixed_method_chaining_contract_for_Product_Collection_composer-2019-10-18-07-02-37.patch [APPLIED]
    Fixed method chaining contract for Product Collection patch for Magento 2.3.3

magento/module-elasticsearch
  ~ rmg/magento2-patches: patches/Catalog_pagination_issue_on_Elasticsearch_6_composer-2019-10-11-08-07-41.patch [APPLIED]
    Resolve ElasticSearch 6 Issues
```

## How to add new patches:
@todo Add walk through of how to add new patches

## Troubleshooting
If you run into issues with a patch installing, you can check the output more verbosely by running:

```bash
composer patch:apply -vvv
```

You should also ensure that you are running a recent version of Composer:

```bash
composer self-update
```

Next, review packages.json and compare the patch you are having issues with to other patches. 
Are the "level" and "cwd" settings there? Are there any differences from a working one?

You can also produce the same patch file by comparing the old version of what the files were 
with what they should be with this command:

```bash
# Compare directories
diff -ru old/ new/ > my.patch

# Compare files
diff -ru old.txt new.txt > my.patch
```

This can be applied with this command to confirm that it actually works:

```bash
patch -p1 < my.patch
```
